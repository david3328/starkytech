const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');
const ps = require('pg-promise').PreparedStatement;
const db = require('../database/db');



const signIn = (req, res) => {
  let user = req.body.username;
  let pass = req.body.password;
  const query = 'SELECT fn_autenticar AS response FROM fn_autenticar($1,$2)';
  const login = new ps('login', query, [user, pass]);
  db.one(login)
    .then(data => {
      let cert = fs.readFileSync(path.join(__dirname, '../keys/private.key'));
      let token = jwt.sign(data.response, cert, { algorithm: 'RS256' });
      res.status(200).json({ success: true, token,data:data.response });
    })
    .catch(err => {
      res.status(400).json({ success: false, message: 'Credenciales incorrectas', err });
    })
}



const auth = (req, res, next) => {
  if (!req.headers['authorization']) return res.status(403).json({ success: false, message: 'No tienes autorización.' });
  const token = req.headers['authorization'];
  let cert = fs.readFileSync(path.join(__dirname, '../keys/public.key'));
  jwt.verify(token, cert, (err, data) => {
    if (err) return res.status(403).json({ success: false, message: 'Token invalido' });
    next();
  })

}

module.exports = { signIn, auth }