const express = require('express');
const router = express.Router();

const {
  crearAlumno,
  listarAlumnos,
  enviarMensaje
} = require('../controllers/alumnosController');

router.post('/alumno',crearAlumno);
router.get('/alumno',listarAlumnos);
router.post('/mensaje',enviarMensaje);

module.exports = router;