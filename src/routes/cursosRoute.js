const express = require('express');
const router = express.Router();

const {
  crearCurso,
  listarCursos,
  listarContenidos,
  listarClases
} = require('../controllers/cursosController');

router.post('/curso',crearCurso);
router.get('/curso',listarCursos);
router.get('/curso/contenidos/:idcurso',listarContenidos);
router.get('/curso/clases/:idcurso',listarClases);

module.exports = router;