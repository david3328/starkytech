const express = require('express');
const router = express.Router();
const {signIn} = require('../services/auth');

router.post('/auth',signIn);


module.exports = router;