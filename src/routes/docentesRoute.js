const express = require('express');
const router = express.Router();

const {
  crearDocente,
  listarDocentes,
  listarCursos
} = require('../controllers/docentesController');

router.post('/docente',crearDocente);
router.get('/docente',listarDocentes);
router.get('/docente/curso/:iddocente',listarCursos)

module.exports = router;