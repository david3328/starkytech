const express = require('express');
const fileUpload = require('express-fileupload');
const path = require('path');
const cors = require('cors');

const app = express();

//Import Routes
const loginRouter = require('./routes/loginRoute');
const cursoRouter = require('./routes/cursosRoute');
const docenteRouter = require('./routes/docentesRoute');
const alumnoRouter = require('./routes/alumnosRoute');

//Settings
app.set('port',process.env.PORT || 3000);

//Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(express.static(path.join(__dirname,'/uploads')));
app.use(fileUpload());

//Use routes
app.use('/api',loginRouter);
app.use('/api',cursoRouter);
app.use('/api',docenteRouter);
app.use('/api',alumnoRouter);

app.listen(app.get('port'),()=>{
  console.log(`Server run on port ${app.get('port')}`);
})