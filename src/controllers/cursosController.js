const ps = require('pg-promise').PreparedStatement;
const db = require('../database/db');
const path = require('path');

exports.crearCurso = (req, res) => {
  let iddocente = req.body.iddocente;
  let titulo = req.body.titulo;
  let slogan = req.body.slogan;
  let descripcion = req.body.descripcion;
  let duracion = req.body.duracion;
  let clases = req.body.clases;
  let imagen = req.files.photo;
  let query = `INSERT INTO cursos (id_docente,titulo,slogan,descripcion,duracion) VALUES ($1,$2,$3,$4,$5) RETURNING id_curso`;
  let insertCurso = new ps('insertCurso', query, [iddocente, titulo, slogan, descripcion, duracion]);
  db.one(insertCurso)
    .then(data => {
      imagen.mv(path.join(__dirname, `../uploads/cursos/${data.id_curso}.jpg`), err => {
        if (err) return res.status(500).json({ success: false, message: err.message, err })
        clases = JSON.parse(clases);
        clases.map(el => {
          query = `INSERT INTO clases (id_curso,video,nombre,duracion)  VALUES ($1,$2,$3,$4)`;
          let insertClase = new ps('insertClase', query, [data.id_curso, el.video, el.clase, el.duracion])
          db.none(insertClase)
        })
        res.status(200).json({ success: true});
      })

    })
    .catch(err => {
      console.log('ERROR: ', err.message || err);
      res.status(400).json({ success: false, message: err.message, err });
    })


}


exports.listarCursos = (req, res) => {
  let query = `SELECT id_curso,titulo,slogan,descripcion,duracion,nombres||' '||apellidos docente,especialidad,d.id_docente FROM cursos c
  INNER JOIN docentes d ON d.id_docente = c.id_docente`
  let listaCursos = new ps('listaCursos', query);
  db.any(listaCursos)
    .then(data => {
      res.status(200).json({ success: true, data });
    })
    .catch(err => {
      console.log('ERROR: ', err.message || err);
      res.status(400).json({ success: false, message: err.message, err });
    })
}

exports.listarContenidos = (req, res) => {
  let idcurso = req.params.idcurso;
  let query = `SELECT id_contenido,caracteristica FROM contenidos WHERE id_curso=$1`;
  let listaContenidos = new ps('listaContenidos', query, idcurso);
  db.any(listaContenidos)
    .then(data => {
      res.status(200).json({ success: true, data });
    })
    .catch(err => {
      console.log('ERROR: ', err.message || err);
      res.status(400).json({ success: false, message: err.message, err });
    })
}

exports.listarClases = (req, res) => {
  let idcurso = req.params.idcurso;
  let query = `SELECT id_clase,video,nombre,duracion FROM clases WHERE id_curso=$1`;
  let listaClases = new ps('listaClases', query, idcurso);
  db.any(listaClases)
    .then(data => {
      res.status(200).json({ success: true, data });
    })
    .catch(err => {
      console.log('ERROR: ', err.message || err);
      res.status(400).json({ success: false, message: err.message, err });
    })
}