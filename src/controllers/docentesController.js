const ps = require('pg-promise').PreparedStatement;
const db = require('../database/db');
const path = require('path');

exports.crearDocente = (req,res)=>{
  console.log(req.body);
  let nombres = req.body.nombres;
  let apellidos = req.body.apellidos;
  let email = req.body.email;
  let telefono = req.body.telefono;
  let especialidad = req.body.especialidad;
  let password = req.body.password;
  let imagen  = req.files.photo;
  db.func('fn_crear_docente',[email,password,nombres,apellidos,email,telefono,especialidad])
  .then(data=>{
    imagen.mv(path.join(__dirname, `../uploads/docentes/${data[0].fn_crear_docente}.jpg`), err => {
      if (err) return res.status(500).json({ success: false, message: err.message, err })
      return res.status(200).json({success:true});
    })
  })
  .catch(err=>{
    console.log('ERROR: ',err.message || err);
    res.status(400).json({success:false,message:err.message,err});
  })
}

exports.listarDocentes = (req,res)=>{
  let query = `SELECT id_docente, nombres||' '||apellidos docente, email, telefono,especialidad FROM docentes`;
  let listaDocentes = new ps('listaDocentes',query);
  db.any(listaDocentes)
  .then(data=>{
    res.status(200).json({success:true,data});
  })
  .catch(err=>{
    console.log('ERROR: ',err.message || err);
    res.status(400).json({success:false,message:err.message,err});
  })
}

exports.listarCursos = (req,res)=>{
  let iddocente = req.params.iddocente;
  let query = `SELECT id_curso,titulo,slogan,descripcion,duracion FROM cursos WHERE id_docente=$1`;
  let cursosDocente = new ps('cursosDocente',query,iddocente);
  db.any(cursosDocente)
  .then(data=>{
    res.status(200).json({success:true,data});
  })
  .catch(err=>{
    console.log('ERROR: ',err.message || err);
    res.status(400).json({success:false,message:err.message,err});
  })
} 