const ps = require('pg-promise').PreparedStatement;
const db = require('../database/db');

exports.crearAlumno = (req,res)=>{
  let nombres = req.body.nombres;
  let apellidos = req.body.apellidos;
  let email = req.body.email;
  let telefono = req.body.telefono;
  let password = req.body.password;
  db.func('fn_crear_alumno',[email,password,nombres,apellidos,email,telefono])
  .then(()=>{
    res.status(200).json({success:true});
  })
  .catch(err=>{
    console.log('ERROR: ',err.message || err);
    res.status(400).json({success:false,message:err.message,err});
  })
}

exports.listarAlumnos = (req,res)=>{
  let query = `SELECT nombres||' '||apellidos alumno, email, telefono, s.inicio,s.fin
  FROM alumnos a
  LEFT JOIN suscripciones s ON s.id_alumno = a.id_alumno`;
  db.any(query)
  .then(data=>{
    res.status(200).json({success:true,data});
  })
  .catch(err=>{
    console.log('ERROR: ',err.message || err);
    res.status(400).json({success:false,message:err.message,err});
  })
}

exports.enviarMensaje = (req,res)=>{
  let nombres = req.body.nombres;
  let correo = req.body.correo;
  let mensaje = req.body.mensaje;
  let query = `INSERT INTO mensajes (nombre,medio,mensaje) VALUES($1,$2,$3)`;
  let insertMessage = new ps('insertMessage',query,[nombres,correo,mensaje]);
  db.none(insertMessage)
  .then(()=>{
    res.status(200).json({success:true})
  })
  .catch(err=>{
    console.log('ERROR: ',err.message || err);
    res.status(400).json({success:false,message:err.message,err});
  })
}