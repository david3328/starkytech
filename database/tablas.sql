﻿CREATE TABLE login(
	id_login smallserial,
	username varchar NOT NULL,
	password varchar NOT NULL,
	CONSTRAINT pk_login PRIMARY KEY (id_login),
	CONSTRAINT uk_login_username UNIQUE (username)
)

CREATE TABLE administradores(
	id_administrador smallserial,
	nombres varchar NOT NULL,
	apellidos varchar NOT NULL,
	email varchar NOT NULL,
	telefono varchar NOT NULL,
	id_login smallint NOT NULL,
	CONSTRAINT pk_administradores PRIMARY KEY (id_administrador),
	CONSTRAINT fk_administradores_login FOREIGN KEY (id_login)
	REFERENCES login(id_login)
)

CREATE TABLE alumnos(
	id_alumno smallserial,
	nombres varchar NOT NULL,
	apellidos varchar NOT NULL,
	email varchar NOT NULL, --UNIQUE
	telefono varchar NOT NULL, --UNIQUE
	id_login smallint,
	CONSTRAINT pk_alumnos PRIMARY KEY (id_alumno),
	CONSTRAINT uk_alumnos_email UNIQUE (email),
	CONSTRAINT uk_alumnos_telefono UNIQUE (telefono),
	CONSTRAINT fk_alumnos_login FOREIGN KEY (id_login)
	REFERENCES login(id_login)
)

CREATE TABLE docentes(
	id_docente smallserial,
	nombres varchar NOT NULL,
	apellidos varchar NOT NULL,
	email varchar NOT NULL,
	telefono varchar NOT NULL,
	especialidad varchar NOT NULL,
	id_login smallint,
	imagen varchar,
	CONSTRAINT pk_docentes PRIMARY KEY (id_docente),
	CONSTRAINT uk_docentes_email UNIQUE (email),
	CONSTRAINT uk_docentes_telefono UNIQUE (telefono),
	CONSTRAINT fk_docentes_login FOREIGN KEY (id_login)
	REFERENCES login(id_login)
)

SELECT * FROM docentes
SELECT id_docente, nombres||' '||apellidos docente, email, telefono,especialidad FROM docentes

CREATE TABLE cursos(
	id_curso smallserial,
	id_docente smallint NOT NULL,
	titulo varchar NOT NULL,
	slogan varchar NOT NULL,
	descripcion text NOT NULL,
	duracion varchar NOT NULL,
	imagen varchar,
	CONSTRAINT pk_curos PRIMARY KEY (id_curso),
	CONSTRAINT fk_cursos_docente FOREIGN KEY (id_docente)
	REFERENCES docentes (id_docente)
)

CREATE TABLE contenidos(
	id_contenido smallserial,
	id_curso smallint NOT NULL,
	caracteristica varchar NOT NULL,
	CONSTRAINT pk_contenidos PRIMARY KEY (id_contenido),
	CONSTRAINT fk_contenidos_curso FOREIGN KEY (id_curso)
	REFERENCES cursos (id_curso)
)

DROP TABLE clases

CREATE TABLE clases(
	id_clase smallserial,
	id_curso smallint NOT NULL,
	video varchar NOT NULL,
	nombre varchar NOT NULL,
	duracion varchar NOT NULL,
	CONSTRAINT pk_clases PRIMARY KEY (id_clase),
	CONSTRAINT fk_clases_curso FOREIGN KEY (id_curso)
	REFERENCES cursos (id_curso)
)

CREATE TABLE matriculas(
	id_matricula smallserial,
	id_alumno smallint NOT NULL,
	id_curso smallint NOT NULL,
	CONSTRAINT pk_matriculas PRIMARY KEY (id_matricula),
	CONSTRAINT fk_matriculas_alumno FOREIGN KEY (id_alumno)
	REFERENCES alumnos (id_alumno),
	CONSTRAINT fk_matriculas_curso FOREIGN KEY (id_curso)
	REFERENCES cursos (id_curso)
)

CREATE TABLE suscripciones(
	id_suscripcion smallserial,
	id_alumno smallint NOT NULL,
	inicio date NOT NULL,
	fin date NOT NULL,
	CONSTRAINT pk_suscripciones PRIMARY KEY (id_suscripcion),
	CONSTRAINT fk_suscripciones_alumno FOREIGN KEY (id_alumno)
	REFERENCES alumnos (id_alumno)
)

DROP TABLE contenidos;
DROP TABLE matriculas;
DROP TABLE clases;
DROP TABLE cursos;
DROP TABLE docentes;

SELECT id_curso,titulo,slogan,descripcion,duracion FROM cursos WHERE id_docente=2