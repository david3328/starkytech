﻿SELECT * FROM login
SELECT * FROM administradores
INSERT INTO login(username,password) VALUES ('rafael@gmail.com','rafaelmendoza'),('gonzales@gmail.com','gonzalescortez')
UPDATE login SET password = md5('rafaelmendoza') WHERE id_login = 11

INSERT INTO administradores(nombres,apellidos,email,telefono,id_login)
VALUES ('Richard','Gonzales','gonzales@gmail.com','947574508',12),
('Rafael','Mendoza','rafael@gmail.com','967432907',11)

CREATE OR REPLACE FUNCTION fn_autenticar(
	_username varchar,
	_password varchar
)
RETURNS json AS 
$BODY$
DECLARE _response json;
BEGIN
	WITH usuarios AS (SELECT a.id_alumno AS id, a.nombres, a.apellidos, l.username, l.password, '0' AS type
	FROM alumnos a 
	INNER JOIN login l ON l.id_login = a.id_login
	UNION
	SELECT d.id_docente AS id, d.nombres, d.apellidos, l.username, l.password, '1' AS type 
	FROM docentes d
	INNER JOIN login l ON l.id_login = d.id_login
	UNION 
	SELECT a.id_administrador AS id, a.nombres, a.apellidos, l.username, l.password, '2' AS type
	FROM administradores a
	INNER JOIN login l ON l.id_login = a.id_login)
	SELECT row_to_json(row(id,nombres,apellidos,username,type)) INTO _response FROM usuarios
	WHERE username = _username AND password = md5(_password);
	IF NOT FOUND THEN
		RAISE EXCEPTION 'Error, usuario no encontrado';
	END IF;
	RETURN _response;
END;
$BODY$
LANGUAGE plpgsql

DROP FUNCTION fn_crear_docente(varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar)

--CREAR DOCENTE
CREATE OR REPLACE FUNCTION fn_crear_docente(
	_username varchar,
	_password varchar,
	_nombres varchar,
	_apellidos varchar,
	_email varchar,
	_telefono varchar,
	_especialidad varchar
)
RETURNS smallint AS 
$BODY$
DECLARE _login smallint; _iddocente smallint;
BEGIN
	INSERT INTO login(username,password) VALUES (_username,md5(_password))
	RETURNING id_login INTO _login;
	IF FOUND THEN
		INSERT INTO docentes(nombres,apellidos,email,telefono,especialidad,id_login)
		VALUES (_nombres,_apellidos,_email,_telefono,_especialidad,_login) RETURNING id_docente INTO _iddocente;
	ELSE
		RAISE EXCEPTION 'No se pudo insertar';
	END IF;
	RETURN _iddocente;
END;
$BODY$
LANGUAGE plpgsql;




SELECT * FROM docentes


--CREAR ALUMNO
DROP FUNCTION fn_crear_alumno(varchar,varchar,varchar,varchar,varchar,varchar)

CREATE OR REPLACE FUNCTION fn_crear_alumno(
	_username varchar,
	_password varchar,
	_nombres varchar,
	_apellidos varchar,
	_email varchar,
	_telefono varchar
)
RETURNS void AS 
$BODY$
DECLARE _login smallint;
BEGIN
	INSERT INTO login(username,password) VALUES (_username,md5(_password))
	RETURNING id_login INTO _login;
	IF FOUND THEN
		INSERT INTO alumnos(nombres,apellidos,email,telefono,id_login)
		VALUES (_nombres,_apellidos,_email,_telefono,_login);
	END IF;
END;
$BODY$
LANGUAGE plpgsql;



--LISTAR CURSOS 
SELECT * FROM cursos
SELECT id_curso,titulo,slogan,descripcion,duracion,nombres,apellidos,d.id_docente FROM cursos c
INNER JOIN docentes d ON d.id_docente = c.id_docente

SELECT nombres||' '||apellidos alumno, email, telefono, s.inicio,s.fin
FROM alumnos a
LEFT JOIN suscripciones s ON s.id_alumno = a.id_alumno


SELECT 



SELECT * FROM cursos 
SELECT * FROM clases 
SELECT * FROM alumnos
SELECT * FROM login
SELECT * FROM suscripciones
SELECT * FROM mensajes